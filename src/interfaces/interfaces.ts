export interface IForm {
	factor: number;
}

export interface ISetFactor {
	factor: number;
	setFactor: (x: number) => void;
}

export interface IInput {
	value: string | number;
	setData?: (x: string) => void;
	handelGet: () => void;
	text: string;
}

export interface ICat {
	type: string;
	text: string;
	fontSize: string;
	color: string;
	filter: string;
	width: string;
	height: string;
}
