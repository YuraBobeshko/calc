import React from 'react';

import { ISetFactor } from '../../interfaces/interfaces';

const arr: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 10];

const SetFactor: React.FC<ISetFactor> = ({ setFactor, factor }) => {
	return (
		<div className={'mb-3'}>
			{arr.map(button => (
				<button
					key={button}
					className={`btn btn-success ${factor === button && 'active'}`}
					onClick={() => setFactor(button)}
				>
					{button}
				</button>
			))}
			<input
				className={'form-control'}
				value={factor}
				onChange={event => setFactor(+event.target.value)}
			/>
		</div>
	);
};

export default SetFactor;
