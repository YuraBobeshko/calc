import React, { useState } from 'react';

import Form from './components/Form/Form';
import SetFactor from './components/SetFactor/SetFactor';
import CatImg from './components/CatImg/CatImg';
import './App.css';


const App: React.FC = () => {
	const [factor, setFactor] = useState<number>(1);

	return (
		<div className="form">
			<h3 className="mb-5">Dasha, you are the best!</h3>
			<Form factor={factor} />
			<SetFactor setFactor={setFactor} factor={factor} />
			<CatImg />
		</div>
	);
};

export default App;
