const getSum = (data: any, factor: number): number => {
  return (
    (data
      .split(" ")
      .reduce((prevValue: string, item: string): number => +prevValue * +item) *
      factor)
      / 1e+6
  );
};

export default getSum;
