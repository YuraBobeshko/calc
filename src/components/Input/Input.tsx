import React from 'react';

import { IInput } from '../../interfaces/interfaces';

const Input: React.FC<IInput> = ({ value, setData, handelGet, text }) => {
	return (
		<div className={'d-flex mb-3'}>
			<input
				className={'form-control'}
				value={value}
				onChange={event => setData && setData(event.target.value)}
			/>
			<button className={'btn btn-success'} onClick={handelGet}>
				{text}
			</button>
		</div>
	);
};

export default Input;
