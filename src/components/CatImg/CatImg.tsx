import React, { useState, useEffect } from "react";

import { ICat } from "../../interfaces/interfaces";

const data = {} as ICat;
const {
  type = "",
  text = "%20",
  fontSize = "50",
  color = "white",
  filter = "",
  width = "",
  height = ""
} = data;

const CatImg: React.FC = () => {
  const [random, setRandom] = useState(Math.random());
  useEffect(() => {
		const timer = setInterval(() => setRandom(Math.random()), 20000);
		return () => clearTimeout(timer);
	}, []);
  const url = `https://cataas.com/cat/${type}/says/${text}?s=${fontSize}&c=${color}&filter=${filter}&width=${width}&height=${height}&uniqueNum=${random}`;
  return (
    <div>
      <img
        style={{ objectFit: "cover", width: "100%", height: "300px" }}
        alt={"cat"}
        src={url}
      />
    </div>
  );
};

export default CatImg;
