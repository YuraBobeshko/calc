const clearString = (data: string): string => {
  return data.match(/\d+/gi)?.join(" ") || "";
};

export default clearString;
