import React, { useState } from 'react';

import Input from '../Input/Input';
import { IForm } from '../../interfaces/interfaces';
import getSum from '../../functions/getSum';
import clearString from '../../functions/clearString';

const Form: React.FC<IForm> = ({ factor }) => {
	const [value, setValue] = useState<string>(' ');
	const enhanceGetSum = (data: string) => getSum(data, factor);
	const handelGet = async () => {
		const nums: string = await navigator.clipboard.readText();
		setValue(clearString(nums));
		navigator.clipboard.writeText('' + enhanceGetSum(clearString(nums)));
	};

	return (
		<>
			<Input
				value={value}
				handelGet={handelGet}
				setData={setValue}
				text={'give'}
			/>
			<Input
				value={enhanceGetSum(value)}
				handelGet={() =>
					navigator.clipboard.writeText(enhanceGetSum(value).toString())
				}
				text={'give'}
			/>
		</>
	);
};

export default Form;
